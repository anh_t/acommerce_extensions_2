<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_CpmsConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */
namespace Acommerce\CPMSConnect\Model\Source;

/**
 * Order Type Option
 *
 * @category Acommerce_CpmsConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@Acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.Acommerce.asia
 */
class OrderType implements \Magento\Framework\Option\ArrayInterface
{

    const STANDARD = 1;
    const DROPSHIP = 2;

    /**
     * Retrive Order Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => self::STANDARD, 'label' => __('Standard')], 
        ['value' => self::DROPSHIP, 'label' => __('Dropship')]];
    }
}
