<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Directory
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\CPMSConnect\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Stock Item Importing Hostory
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class StockImport extends AbstractModel
{

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\CPMSConnect\Model\ResourceModel\StockImport');

    }//end _construct()
    //@codingStandardsIgnoreEnd

}//end class