<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Directory
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */

namespace Acommerce\CPMSConnect\Model;

use Magento\Framework\ObjectManagerInterface;

/**
 * Stock Item Importing History Factory
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class StockImportFactory
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;


    /**
     * Construct
     *
     * @param ObjectManagerInterface $objectManager Object Manager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;

    }//end __construct()


    /**
     * Create Stock Item Importing History Model
     *
     * @param array $arguments Arguments
     *
     * @return StockImport
     */
    public function create(array $arguments=array())
    {
        return $this->objectManager->create(
            'Acommerce\CPMSConnect\Model\StockImport', $arguments
        );

    }//end create()


}//end class
