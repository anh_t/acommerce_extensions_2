<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container as GridContainer;

/**
 * Sales Order Exporting History Widget Grid Container
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Orderexport extends GridContainer
{
    // @codingStandardsIgnoreStart
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {

        $this->_controller = 'adminhtml_orderexport';/*block grid.php directory*/
        $this->_blockGroup = 'Acommerce_CPMSConnect';
        $this->_headerText = __('Sales Order Exporting History');
        parent::_construct();
        $this->removeButton('add');
    }
    // @codingStandardsIgnoreEnd
}